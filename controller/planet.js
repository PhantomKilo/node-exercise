const express = require("express");
const router = express.Router();
const swapi = require("swapi-node");

router.get("/", async (req, res) => {
  const url = "https://swapi.dev/api/planets/?page=";
  let page = 1;
  let planets = [];
  let lastResult = [];
  do {
    try {
      const resp = await swapi.get(url + page);
      lastResult = resp;
      await resp.results.forEach((planet) => {
        let i = 0;
        planet.residents.forEach(async (resident, i) => {
          const result = await swapi.get(resident);
          console.log(result.name);
          planet.residents[i] = result.name
          i++
        });
        planets.push(planet);
      });
      page++;
    } catch (e) {
      console.log(e);
    }
  } while (lastResult.next != null);
  await res.json(planets);
});

module.exports = router;
