const express = require('express')
const app = express()
const PORT = 4000
const peopleRouter = require('./controller/people')
const planetRouter = require('./controller/planet')

app.use(express.json())

app.use('/people', peopleRouter)
app.use('/planets', planetRouter)

app.set("port", PORT)
app.listen(app.get('port'), () => {
    console.log(`Port ${app.get('port')}`)
})