const express = require("express");
const router = express.Router();
const swapi = require("swapi-node");

router.get("/", async (req, res) => {
  const url = "https://swapi.dev/api/people/?page=";
  let page = 1;
  const sort = req.query.sortBy;
  let people = [];
  let lastResult = [];
  do {
    try {
      const resp = await swapi.get(url + page);
      lastResult = resp;
      await resp.results.forEach((person) => {
        people.push(person);
      });

      page++;
    } catch (e) {
      console.log(e);
    }
  } while (lastResult.next != null);

  if (sort == "name") {
    const compare = (a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    };
    await res.json(people.sort(compare));
  } else if (sort == "height") {
    await res.json(people.sort((a, b) => Number(a.height) - Number(b.height)));
  } else if (sort == "mass") {
    await res.json(people.sort((a, b) => Number(a.mass) - Number(b.mass)));
  } else {
    await res.json(people);
  }
});

module.exports = router;
